#!/bin/bash

remote="gdrive_cloud"
declare -a strtail=""

if [ -a /tmp/mygdrivesync.log ]; then
	last=$(tail -1 /tmp/mygdrivesync.log)

	#update last sync
	while read -r time date action filestat
	do
		let len=$(echo -n $time | wc -c)+$(echo -n $date | wc -c)+$(echo -n $action | wc -c)+3
		let filestatlen=$(echo -n $filestat | wc -c)
		# [[ $filestat =~ .*[OK]] ]]
		# if [ ! "$?" -eq 0 ]; then
		# 	[[ $filestat =~ .*[FAIL]] ]]
		# 	if [ ! "$?" -eq 0 ]; then
		# 		let filestatlen=filestatlen+6
		# 	fi	
		# fi
		let sum=len+filestatlen

		if [ "$sum" -leq 80 ]; then
			strtail=$strtail$'\n'$time$' '$date$' '$action$' '$filestat
		else
			let remaining=70-len
			let start=filestatlen-remaining
			let end=filestatlen
			strtail=$strtail$'\n'$time$' '$date$' '$action$' '$' ....'$(echo $filestat | cut -c $start-$end)$' '$stat
		fi
	done < <(tail -10 /tmp/mygdrivesync.log)
	strtail="Last Sync:"$'\n'${strtail}$'\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'
	#echo '<click>xfce4-terminal -T "Sync Activity" -e "tail -50 /tmp/mygdrivesync.log"</click>'
	echo '<click>xterm -ls -xrm "XTerm*selectToClipboard: true" -geometry 150x24 -bg black -fg white -T "Sync Log" -hold -e "cat /tmp/mygdrivesync.log"</click>'

	#update transaction summary
	# let trans=0
	# let success=0
	# let fail=0
	# while read -r line
	# do
	# 	[[ $line =~ .*[OK]] ]]
	# 	if [ "$?" -eq 0 ]; then
	# 		let success=success+1	
	# 		let trans=trans+1
	# 	else
	# 		[[ $line =~ .*[FAIL]] ]]
	# 		if [ "$?" -eq 0 ]; then
	# 			let fail=fail+1
	# 			let trans=trans+1
	# 		fi	
	# 	fi
	# done < <(cat /tmp/mygdrivesync.log)

	#update transaction summary
	let ok=0
	let fail=0
	if [ -a /tmp/mygdrivesync_sum.log ]; then
		while read -r readok readfail 
		do
			let ok=readok
			let fail=readfail
		done < <(tail -1 /tmp/mygdrivesync_sum.log)	
	fi
	let trans=ok+fail
	
	strtail=${strtail}$'\n'"Summary: "$'\n\n'${trans}$' transactions ('${ok}$' succeed, '${fail}$' failed)'

	#update icon
	[[ $last =~ .*[OK]] ]]
	if [ "$?" -eq 0 ];then
	    echo "<img>/usr/share/icons/google-drive.png</img>"
	else
		[[ $last =~ .*[FAIL]] ]]
		if [ "$?" -eq 0 ];then
			echo "<img>/usr/share/icons/google-drive-fail.png</img>"		    
		else
			[[ $last =~ .*started... ]]
			if [ "$?" -eq 0 ];then
				echo "<img>/usr/share/icons/google-drive.png</img>"
			else
				echo "<img>/usr/share/icons/google-drive-insync.png</img>"	
			fi
			
		fi
	fi
	
else
	strtail="Sync backend not started"
	echo "<img>/usr/share/icons/google-drive-fail.png</img>"	
	echo "<click> xterm -bg black -fg white -T 'Sync Log' -hold -e 'echo No Sync Log Yet'</click>"
fi
echo "<tool>$strtail</tool>"


