#!/bin/bash

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Lamuk
# Full cloud storage autosync by Nurkholish AF
# based on rclone and inotify
# Original idea: Author Leo G (techarena51.com), modified for rclone by bobbintb

#DECLARE VARIABLES
backup_dir="/home/ardi/media/cloud/"
server_backup="gdrive_cloud"
server_backup_dir="gdrive_cloud:/CloudBacked/"

function remotesize {
	echo "Examining storage"
	if [ -a /tmp/mygdrivesync.remotesize ]; then
		rm /tmp/mygdrivesync.remotesize
	fi
	rclone size gdrive_cloud |
		while read str
		do
			echo $str >> /tmp/mygdrivesync.remotesize
		done
}

function log_activity {
	echo $(date "+%H:%M:%S %d-%m-%Y") $1 $2 >> /tmp/mygdrivesync.log
}

function log_status {
	if [ "$?" -eq 0 ]; then
		sed -i '${s/$/ [OK]/}' /tmp/mygdrivesync.log
		log_summary 0
	else
		sed -i '${s/$/ [FAIL]/}' /tmp/mygdrivesync.log
		log_summary 1
	fi	
}

function log_summary {
	let ok=0
	let fail=0
	if [ -a /tmp/mygdrivesync_sum.log ]; then
		while read -r readok readfail 
		do
			let ok=readok
			let fail=readfail
		done < <(tail -1 /tmp/mygdrivesync_sum.log)	
	fi
	if [ "$1" -eq 0 ]; then
		let ok=ok+1
	else
		let fail=fail+1
	fi
	echo $ok $fail > /tmp/mygdrivesync_sum.log
}

#My own man pages say nothing about what 0 means, but from digging around, it seems to mean the current process group. Since the script get's it's own process group, this ends up sending SIGHUP to all the script's children, foreground and background.
#http://stackoverflow.com/questions/1644856/terminate-running-commands-when-shell-script-is-killed
trap 'kill -HUP 0' EXIT

#CHECK IF INOTIFY-TOOLS IS INSTALLED
type -P inotifywait &>/dev/null || { echo "inotifywait command not found."; exit 1; }

sleep 10

notify-send -t 5000 --icon="/usr/share/icons/Numix-Circle/48/apps/google-drive.svg" "Google Drive" "Synchronization running in background" 

movefrom=""
echo "#############################################"
echo "Standby...."
echo $(date "+%H:%M:%S %d-%m-%Y") "Synchronization started..." > /tmp/mygdrivesync.log
echo "0 0" > /tmp/mygdrivesync_sum.log

inotifywait -m -r -e close_write,move,create,delete --format '%e "%w%f"' $backup_dir |
	while read eventlist
	do
		#START RCLONE AND ENSURE DIR ARE UPTO DATA
		#sync  || exit 0

		#START RCLONE AND TRIGGER BACKUP ON CHANGE
		
		action=${eventlist%% *}
		filepath=$(echo $eventlist | cut -d "\"" -f2 | cut -d "\"" -f1)
		echo "Action=$eventlist"
		dest=${filepath#$backup_dir}
		dest=$server_backup_dir$dest
		case "$action" in
		"CREATE" | "CLOSE_WRITE" | "CLOSE_WRITE,CLOSE")
			dest=${dest%$(basename "$filepath")} #delete basename
			echo "copying $filepath $dest"
			log_activity [$action] "$filepath"
			rclone copy "$filepath" "$dest" --contimeout=10s
			log_status
		    ;;
		"CREATE,ISDIR")
			log_activity [$action] "$filepath"
			rclone mkdir "$dest" --contimeout=10s
			log_status
		    ;;
		"DELETE")
			log_activity [$action] "$filepath"
			rclone delete "$dest" --contimeout=10s
			log_status
		    ;;
	 	"DELETE,ISDIR")
			echo "Deleting dir " "$dest"
			log_activity [$action] "$filepath"
			rclone rmdir "$dest" --contimeout=10s
			log_status
		    ;;
		"MOVED_FROM")
			echo "MF"
			movefrom=$dest
			log_activity [$action] "$filepath"
		    ;;
		"MOVED_TO")
			echo "MT"
			if [ "$movefrom" != "" ]; then
				rclone delete "$movefrom" --contimeout=10s	
				log_status
				dest=${dest%$(basename "$filepath")} #delete basename, create destionation to be copy
		    	movefrom=""
		    	echo "copying $filepath $dest"
				log_activity [$action] "$filepath"
			    rclone copy "$filepath" "$dest" --contimeout=10s
			    log_status
			fi
		    ;;
		*)
		    #do_nothing()
		    ;;
		esac
		echo "#############################################"
		echo "Standby...."

	done